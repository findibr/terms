### Dependencies
Ionic
[window.plugins.toast](https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin)
```html
<script src="lib/terms/terms.js"></script>
```

### Insert module
```javascript
angular.module('myModule', ['findi.terms'])
```

### Use
```html
<terms options="options"></terms>
```
### options
```javascript
{
	title: 'STRING',
	apiUrl: 'STRING' // required
	errorMessage: 'STRING'
}
```
