(function() {
	'use strict'

	angular.module('findi.terms', ['ionic'])
		.component('terms', {
			template: '<h1 class="title" ng-if="$ctrl.options.showTitle" ng-bind="$ctrl.title"></h1>' +
				'<div ng-bind-html="$ctrl.terms"></div>',
			controller: termsController,
			controllerAs: '$ctrl',
			bindings: {
				options: '='
			}
		})

	function termsController($scope, $http, $ionicLoading) {
		var vm = this
		let referer = vm.options.referer
		let doc = "terms_of_use_" + vm.options.referer

		vm.title = vm.options.title ||
			'Termos e condições gerais para a utilização do appeventos'

		$ionicLoading.show()
		$http.get(vm.options.apiUrl + '/documents/', {params: {"document": doc}})
			.then(function(success) {
				vm.terms = success.data.document
				$ionicLoading.hide()
			}, function(error) {
				var message = vm.options.errorMessage || 'Erro ao baixar termos de uso.'
				window.plugins.toast.show(message, 'short', 'bottom',
					null, null);
				$ionicLoading.hide()
			})
	}
})()

